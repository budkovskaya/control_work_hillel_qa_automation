package control_work;

public class ControlWork {
    public static void main(String Args[]) {

        // Task 1: If-Else
        ControlWorkMethods.tellNumber1();

        // Task 1: Switch
        ControlWorkMethods.tellNumber2();

        // Task 2: do/while
        ControlWorkMethods.showArray1();

        //Task2: for
        ControlWorkMethods.showArray2();

        // Task2: for each
        ControlWorkMethods.showArray3();

        // Task 3:
        ControlWorkMethods.calculateSum();

        // Task 4:
        ControlWorkMethods.canVote();

        // Task 4: second variant
        ControlWorkMethods.canVote2();

        // Task 5:
        ControlWorkMethods.workWithArray();

        // Task 6:
        ControlWorkMethods.TwoDimArrays();

    }

}
