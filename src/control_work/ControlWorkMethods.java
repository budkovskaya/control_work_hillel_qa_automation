package control_work;

import java.util.Scanner;

public class ControlWorkMethods {

    // Task 1: If-Else
    public static void tellNumber1() {

        //Ask the user to enter a number
        System.out.println("Please enter the number:");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        //Check which number was entered and return result
        if (num == 1) {
            System.out.println("Вы ввели число 1");
        } else if (num == 2) {
            System.out.println("Вы ввели число 2");
        } else if (num == 3) {
            System.out.println("Вы ввели число 3");
        } else {
            System.out.println("Вы ввели не 1, 2, 3");
        }
    }

    //Task 1: Switch
    public static void tellNumber2() {

        //Ask the user to enter a number
        System.out.println("Please enter the number:");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        //Check which number was entered and return result
        switch (number) {
            case 1:
                System.out.println("Вы ввели число 1");
                break;
            case 2:
                System.out.println("Вы ввели число 2");
                break;
            case 3:
                System.out.println("Вы ввели число 3");
                break;
            default:
                System.out.println("Вы ввели не 1, 2, 3");
                break;
        }
    }

    // Task 2: Array: do/while
    public static void showArray1() {
        int arr[] = {4, 9, 15, 7};
        int i = 0;
        do {
            System.out.print(arr[i] + " ");
            i++;
        } while (i < arr.length);
        System.out.print("\n");
    }

    // Task 2: Array: for
    public static void showArray2() {
        int arr[] = {4, 9, 15, 7};
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.print("\n");
    }

    // Task 2: Array: for each
    public static void showArray3() {
        int arr[] = {4, 9, 15, 7};
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.print("\n");
    }

    // Task 3:
    public static void calculateSum() {
        /*Ask the user to enter the number not less than 2. The user will be asked
        to enter the number until he enters 2 or  higher
        */
        int number;
        do {
            System.out.println("Please enter number not less than 2:");
            Scanner scanner = new Scanner(System.in);
            number = scanner.nextInt();
        }while (number < 2);

        int sum = 0;
        for (int i = 2; i <= number; i++) {
            sum += i;
        }
        System.out.println("The sum will be " + sum);

    }

    // Task 4:
    public static void canVote() {

        //Ask the user to enter his/her age
        System.out.println("Please enter your age:");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        //Check which number was entered and return result
        String result = (number < 18) ? "You cannot vote yet!" : "Congrats! You can vote!";
        System.out.println(result);
    }

    // Task 4: second variant
    public static void canVote2() {

        //Ask the user to enter his/her age
        System.out.println("Please enter your age:");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        //Check which number was entered and return result
        String result = (number >= 18) ? "Congrats! You can vote!" : "You cannot vote yet!";
        System.out.println(result);
    }

    // Task 5:
    public static void workWithArray() {
        // Create array with length 4
        String[] fruits = new String[4];

        // Add elements to array
        fruits[0] = "Apple";
        fruits[1] = "Orange";
        fruits[2] = "Lemon";
        fruits[3] = "Plum";

        // Print all names of fruits
        for (String i : fruits) {
            System.out.println("Fruit name is " + i);
        }

        // Print array length
        int arrayLenght = fruits.length;
        System.out.println("Array length is " + arrayLenght);

        // Print fruit names in reverse order
        int i = 3;
        do{
            System.out.println("Fruit name in reverse order is " + fruits[i]);
            i--;
        }while (i >= 0);
    }


    // Task 6:
    public static void TwoDimArrays() {
        int[][] arr = {{1, 7, 9, 10}, {5, 2, 2}};
        for (int i = arr.length - 1; i >= 0 ; i--) {
            for (int j = arr[i].length - 1; j >=0; j--) {
                System.out.print( arr[i][j] + " ");
            }
        }
    }


}





